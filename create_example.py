from pathlib import Path
import os
import numpy as np
import nibabel as nib

# Set path to directory
data_path = Path("...") # path image example
img = nib.load(data_path)
nifti_numpy = img.get_fdata()
nifti_numpy[nifti_numpy!=2]=0 # all voxel -> background except bone
nifti_numpy[nifti_numpy==2]=1 # bone war class 2, now class 1
new_img = nib.Nifti1Image(nifti_numpy, img.affine)
nib.save(new_img, "...") # path image example new