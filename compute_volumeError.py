import torchio as tio
import nibabel as nib
from pathlib import Path
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import math
import os

# comments are for osteophytes

os.environ["KMP_DUPLICATE_LIB_OK"]  =  "TRUE"

num_testing =49 # number of testing cases
sum_volume_1=0
sum_volume_2=0
sum_volume_3=0
sum_volume_4=0
diff_1=np.zeros(4num_testing9)
diff_2=np.zeros(num_testing)
# diff_2_osteo=np.zeros(num_testing)
diff_3=np.zeros(num_testing)
diff_4=np.zeros(num_testing)
# diff_4_osteo=np.zeros(num_testing)
size_bone=np.zeros(num_testing)
# size_osteo=np.zeros(num_testing)

data_path1 = Path("...") # path 2-class nnformer
data_path2 = Path("...") # path 3-class nnformer
data_path3 = Path("...") # path 2-class nnUNet
data_path4 = Path("...") # path 3-class nnUNet
data_path5 = Path("...") # path 3-class ground truth
i=0
for filename in os.listdir(data_path1):
    file_path1=str(data_path1 / filename)
    img1 = nib.load(file_path1)
    nifti_numpy1 = img1.get_fdata()
    pixdim1 = img1.header['pixdim'] # get img size

    file_path2 = str(data_path2 / filename)
    img2 = nib.load(file_path2)
    nifti_numpy2 = img2.get_fdata()
    pixdim2 = img2.header['pixdim']

    file_path3=str(data_path3 / filename)
    img3 = nib.load(file_path3)
    nifti_numpy3 = img3.get_fdata()
    pixdim3 = img3.header['pixdim']

    file_path4=str(data_path4 / filename)
    img4 = nib.load(file_path4)
    nifti_numpy4 = img4.get_fdata()
    pixdim4 = img4.header['pixdim']

    file_path5=str(data_path5 / filename)
    img5 = nib.load(file_path5)
    nifti_numpy5 = img5.get_fdata()
    pixdim5 = img5.header['pixdim']

    # Compute volume
    # for 2-class prediction, class 1: bone
    # for 3-class prediction, class 1: osteophytes, class 2: bone
    nonzero_voxel_count1 = np.count_nonzero(nifti_numpy1==1)
    nonzero_voxel_volume1 = nonzero_voxel_count1 * pixdim1[1] * pixdim1[2] * pixdim1[3]

    nonzero_voxel_count2 = np.count_nonzero(nifti_numpy2==2)
    nonzero_voxel_volume2 = nonzero_voxel_count2 * pixdim2[1] * pixdim2[2] * pixdim2[3]
    # nonzero_voxel_count2_o=np.count_nonzero(nifti_numpy2==1)
    # nonzero_voxel_volume2_o = nonzero_voxel_count2_o * pixdim2[1] * pixdim2[2] * pixdim2[3]

    nonzero_voxel_count3 = np.count_nonzero(nifti_numpy3==1)
    nonzero_voxel_volume3 = nonzero_voxel_count3 * pixdim3[1] * pixdim3[2] * pixdim3[3]

    nonzero_voxel_count4 = np.count_nonzero(nifti_numpy4==2)
    nonzero_voxel_volume4 = nonzero_voxel_count4 * pixdim4[1] * pixdim4[2] * pixdim4[3]
    # nonzero_voxel_count4_o = np.count_nonzero(nifti_numpy4 == 1)
    # nonzero_voxel_volume4_o = nonzero_voxel_count4_o * pixdim4[1] * pixdim4[2] * pixdim4[3]

    nonzero_voxel_count5 = np.count_nonzero(nifti_numpy5 == 2)
    nonzero_voxel_volume5 = nonzero_voxel_count5 * pixdim5[1] * pixdim5[2] * pixdim5[3]
    # nonzero_voxel_count5_o = np.count_nonzero(nifti_numpy5 == 1)
    # nonzero_voxel_volume5_o = nonzero_voxel_count5_o * pixdim5[1] * pixdim5[2] * pixdim5[3]
    size_bone[i]=nonzero_voxel_volume5
    # size_osteo[i]=nonzero_voxel_volume5_o

    # abs. volume error
    diff_1[i]=np.abs(nonzero_voxel_volume5-nonzero_voxel_volume1)
    diff_2[i] = np.abs(nonzero_voxel_volume5 - nonzero_voxel_volume2)
    # diff_2_osteo[i] = nonzero_voxel_volume5_o - nonzero_voxel_volume2_o
    diff_3[i] = np.abs(nonzero_voxel_volume5 - nonzero_voxel_volume3)
    diff_4[i] = np.abs(nonzero_voxel_volume5 - nonzero_voxel_volume4)
    # diff_4_osteo[i] = nonzero_voxel_volume5_o - nonzero_voxel_volume4_o

    i=i+1

# mean abs. volume difference bone
avg1=np.sum(diff_1)/49
avg2=np.sum(diff_2)/49
avg3=np.sum(diff_3)/49
avg4=np.sum(diff_4)/49
# in Percent bone
avg1_per=np.sum(diff_1/size_bone)/49*100
avg2_per=np.sum(diff_2/size_bone)/49*100
avg3_per=np.sum(diff_3/size_bone)/49*100
avg4_per=np.sum(diff_4/size_bone)/49*100

#standard derivation bone
s1=np.std(diff_1/size_bone*100)
s2=np.std(diff_2/size_bone*100)
s3=np.std(diff_3/size_bone*100)
s4=np.std(diff_4/size_bone*100)

# mean abs. volume difference osteophytes
# avg_o2=np.sum(diff_2_osteo)/49
# avg_o4=np.sum(diff_4_osteo)/49
# # in Percent osteophytes
# avg_o2_per=np.sum(diff_2_osteo/size_osteo)/49*100
# avg_o4_per=np.sum(diff_4_osteo/size_osteo)/49*100
# #standard derivation osteophytes
# s2o=np.std(diff_2_osteo/size_osteo*100)
# s4o=np.std(diff_4_osteo/size_osteo*100)

# output 1(nnFormer,2class), 2(nnFormer,3class), 3(nnU-Net,2class), 4(nnU-Net,3class)
print("1(nnFormer,2class), 2(nnFormer,3class), 3(nnU-Net,2class), 4(nnU-Net,3class)")
print("volume error")
print("avg1=", avg1)
print("avg2=", avg2)
print("avg3=", avg3)
print("avg4=", avg4)
print("volume error in %")
print("avg1=", avg1_per)
print("avg2=", avg2_per)
print("avg3=", avg3_per)
print("avg4=", avg4_per)
print("standard derivation")
print("s1=", s1)
print("s2=", s2)
print("s3=", s3)
print("s4=", s4)




