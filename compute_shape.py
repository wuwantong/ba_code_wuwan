from pathlib import Path
import os
import numpy as np
import nibabel as nib

# Set path to directory
data_path = Path("...") #path of images
num=49 # number of images
training_shape1=np.zeros(num)
training_shape2=np.zeros(num)
training_shape3=np.zeros(num)

# Go through each file in directory
i=0
for filename in os.listdir(data_path):
    # Get path to file
    file_path = str(data_path / filename)
    img = nib.load(file_path)
    pixdim = img.header['pixdim']
    training_shape1[i],training_shape2[i],training_shape3[i]=img.dataobj.shape
    i=i+1

# compute mean shape
mean1=np.mean(training_shape1)
mean2=np.mean(training_shape2)
mean3=np.mean(training_shape3)
print(mean1)
print(mean2)
print(mean3)