#!/usr/local_rwth/bin/zsh

### Setup in this script:

### - 1 nodes (c18g, default)

### - 1 rank per node

### - 2 GPUs per rank (= both GPUs from the node)

#SBATCH --job-name="osterphytes"

#SBATCH --output=output.%J.log

#SBATCH --ntasks=1

#SBATCH --mem-per-cpu=100000M

#SBATCH --ntasks-per-node=1

#SBATCH --gres=gpu:1

#SBATCH --time=1-20:50:00
 

# activate environment

cd /home/qb913274/BA/nnFormer

source /home/qb913274/anaconda3/etc/profile.d/conda.sh
conda activate nnFormer
 
# start training
export nnFormer_raw_data_base="/home/qb913274/BA/DATASET/nnFormer_raw"
export nnFormer_preprocessed="/home/qb913274/BA/DATASET/nnFormer_preprocessed"
export RESULTS_FOLDER="/home/qb913274/BA/DATASET/nnFormer_trained_models"

nnFormer_convert_decathlon_task -i ../DATASET/nnFormer_raw/nnFormer_raw_data/Task01_osteophytes
nnFormer_plan_and_preprocess -t 1 --verify_dataset_integrity
nnFormer_train 3d_fullres nnFormerTrainerV2 -c 1 0

nnFormer_predict -i ../DATASET/nnFormer_raw/nnFormer_raw_data/Task001_osteophytes/imagesTs -o ../DATASET/nnFormer_raw/nnFormer_raw_data/Task001_osteophytes/inferTs/output -m 3d_fullres -f 0 -t 1 -chk model_best -tr nnFormerTrainerV2

python ./nnformer/inference.py



