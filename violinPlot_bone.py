import torchio as tio
import nibabel as nib
from pathlib import Path
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import math
import os

os.environ["KMP_DUPLICATE_LIB_OK"]  =  "TRUE"
num_testing =49 # number of testing cases
voxel_array1 = np.zeros(num_testing)
volume_array1 = np.zeros(num_testing)
voxel_array2 = np.zeros(num_testing)
volume_array2 = np.zeros(num_testing)
voxel_array3 = np.zeros(num_testing)
volume_array3 = np.zeros(num_testing)
voxel_array4 = np.zeros(num_testing)
volume_array4 = np.zeros(num_testing)
#sum_volume_testing=0
#sum_voxel_testing=0
sum_volume_diff_2=0
sum_voxel_diff_2=0
data_path1 = Path("...") # path of training visible leg
data_path2 = Path("...") # path of testing visible leg
data_path3 = Path("...") # path of training bone
data_path4 = Path("...") # path of testing bone
data_path5 = Path("...") # path of ground truth
i=0
for filename in os.listdir(data_path1):

    file_path1=str(data_path1 / filename)
    img1 = nib.load(file_path1)
    nifti_numpy1 = img1.get_fdata()
    pixdim1 = img1.header['pixdim']

    file_path2 = str(data_path2 / filename)
    img2 = nib.load(file_path2)
    nifti_numpy2 = img2.get_fdata()
    pixdim2 = img2.header['pixdim']

    file_path3=str(data_path3 / filename)
    img3 = nib.load(file_path3)
    nifti_numpy3 = img3.get_fdata()
    pixdim3 = img3.header['pixdim']

    file_path4=str(data_path4 / filename)
    img4 = nib.load(file_path4)
    nifti_numpy4 = img4.get_fdata()
    pixdim4 = img4.header['pixdim']

    file_path5=str(data_path5 / filename)
    img5 = nib.load(file_path5)
    nifti_numpy5 = img5.get_fdata()
    pixdim5 = img5.header['pixdim']
    # Compute volume
    # for 2-class prediction, class 1: bone
    # for 3-class prediction, class 1: osteophytes, class 2: bone
    # nnFormer, 2-class
    nonzero_voxel_count1 = np.count_nonzero(nifti_numpy1==1)
    nonzero_voxel_volume1 = nonzero_voxel_count1 * pixdim1[1] * pixdim1[2] * pixdim1[3]
    # nnFormer, 3-class
    nonzero_voxel_count2 = np.count_nonzero(nifti_numpy2==2)
    nonzero_voxel_volume2 = nonzero_voxel_count2 * pixdim2[1] * pixdim2[2] * pixdim2[3]
    # nnU-Net, 2-class
    nonzero_voxel_count3 = np.count_nonzero(nifti_numpy3==1)
    nonzero_voxel_volume3 = nonzero_voxel_count3 * pixdim3[1] * pixdim3[2] * pixdim3[3]
    # nnU-Net, 3-class
    nonzero_voxel_count4 = np.count_nonzero(nifti_numpy4==2)
    nonzero_voxel_volume4 = nonzero_voxel_count4 * pixdim4[1] * pixdim4[2] * pixdim4[3]
    # gt
    nonzero_voxel_count5 = np.count_nonzero(nifti_numpy5 == 1)
    nonzero_voxel_volume5 = nonzero_voxel_count5 * pixdim5[1] * pixdim5[2] * pixdim5[3]

    # volume difference
    volume_diff1=nonzero_voxel_volume5-nonzero_voxel_volume1
    voxel_diff1=nonzero_voxel_count5-nonzero_voxel_count1
    volume_diff2 = nonzero_voxel_volume5 - nonzero_voxel_volume2
    voxel_diff2 = nonzero_voxel_count5 - nonzero_voxel_count2
    volume_diff3 = nonzero_voxel_volume5 - nonzero_voxel_volume3
    voxel_diff3 = nonzero_voxel_count5 - nonzero_voxel_count3
    volume_diff4 = nonzero_voxel_volume5 - nonzero_voxel_volume4
    voxel_diff4 = nonzero_voxel_count5 - nonzero_voxel_count4

    voxel_array1[i]=voxel_diff1
    volume_array1[i]=volume_diff1
    voxel_array2[i] = voxel_diff2
    volume_array2[i] = volume_diff2
    voxel_array3[i] = voxel_diff3
    volume_array3[i] = volume_diff3
    voxel_array4[i] = voxel_diff4
    volume_array4[i] = volume_diff4
    i=i+1

# diff in mm^3
ax = sns.violinplot(data=[volume_array1, volume_array2, volume_array3, volume_array4])
# diff in voxel count
# ax = sns.violinplot(data=[voxel_array1, voxel_array2, voxel_array3, voxel_array4])
plt.show()



