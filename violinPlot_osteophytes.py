import torchio as tio
import nibabel as nib
from pathlib import Path
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import math
import os

os.environ["KMP_DUPLICATE_LIB_OK"]  =  "TRUE"
num_testing =49 # number of testing cases
sum_volume_1=0
sum_volume_2=0
sum_volume_3=0
sum_volume_4=0
diff_1=np.zeros(num_testing)
diff_2=np.zeros(num_testing)
diff_2_osteo=np.zeros(num_testing)
diff_3=np.zeros(num_testing)
diff_4=np.zeros(num_testing)
diff_4_osteo=np.zeros(num_testing)
size_bone=np.zeros(num_testing)
size_osteo=np.zeros(num_testing)

data_path1 = Path("...") # path of training visible leg
data_path2 = Path("...") # path of testing visible leg
data_path3 = Path("...") # path of training bone
data_path4 = Path("...") # path of testing bone
data_path5 = Path("...") # path of ground truth
i=0
for filename in os.listdir(data_path1):

    file_path2 = str(data_path2 / filename)
    img2 = nib.load(file_path2)
    nifti_numpy2 = img2.get_fdata()
    pixdim2 = img2.header['pixdim']

    file_path4=str(data_path4 / filename)
    img4 = nib.load(file_path4)
    nifti_numpy4 = img4.get_fdata()
    pixdim4 = img4.header['pixdim']

    file_path5=str(data_path5 / filename)
    img5 = nib.load(file_path5)
    nifti_numpy5 = img5.get_fdata()
    pixdim5 = img5.header['pixdim']
    # Compute volume
    # for 2-class prediction, class 1: bone
    # for 3-class prediction, class 1: osteophytes, class 2: bone
    # nnFormer, 3-class
    nonzero_voxel_count2_o=np.count_nonzero(nifti_numpy2==1)
    nonzero_voxel_volume2_o = nonzero_voxel_count2_o * pixdim2[1] * pixdim2[2] * pixdim2[3]
    # nnU-Net, 3-class
    nonzero_voxel_count4_o = np.count_nonzero(nifti_numpy4 == 1)
    nonzero_voxel_volume4_o = nonzero_voxel_count4_o * pixdim4[1] * pixdim4[2] * pixdim4[3]
    # ground truth
    nonzero_voxel_count5_o = np.count_nonzero(nifti_numpy5 == 1)
    nonzero_voxel_volume5_o = nonzero_voxel_count5_o * pixdim5[1] * pixdim5[2] * pixdim5[3]
    # volume gt
    size_osteo[i]=nonzero_voxel_volume5_o

    # volume diff
    diff_2_osteo[i] = nonzero_voxel_volume5_o - nonzero_voxel_volume2_o
    diff_4_osteo[i] = nonzero_voxel_volume5_o - nonzero_voxel_volume4_o

    i=i+1

ax = sns.violinplot(data=[diff_2_osteo, diff_4_osteo])
plt.show()



