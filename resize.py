import torchio as tio
import nibabel as nib
from pathlib import Path
import os

os.environ["KMP_DUPLICATE_LIB_OK"]  =  "TRUE"
# Set path to directory
data_path = Path("D:/datas/labels/osteophytes") #path of original images and labels

# Go through each file in directory
for filename in os.listdir(data_path):
    # Get path to file
    file_path = str(data_path / filename)
    image = tio.ScalarImage(file_path)
    # resize to 1/2 in each dimension
    red_x = image.spacing[0] * 2
    red_y = image.spacing[1] * 2
    red_z = image.spacing[2] * 2
    # transform = tio.CropOrPad((128,128,100))
    transform = tio.Resample((red_x, red_y, red_z))
    output = transform(image)
    # nifti_numpy = output.get_data()
    # output = nib.Nifti1Image(output.data, output.affine)
    output.save("D:/datas/labels/osteophytes_new/%s.nii.gz" % filename)
    # nib.save(new_img, "C:/Users/Stacey/Desktop/test/%s.nii.gz" % filename)

