#!/usr/local_rwth/bin/zsh

### Setup in this script:

### - 1 nodes (c18g, default)

### - 1 rank per node

### - 2 GPUs per rank (= both GPUs from the node)

#SBATCH --account=rwth0536

#SBATCH --job-name="osterphytes"

#SBATCH --output=output.%J.log

#SBATCH --ntasks=1

#SBATCH --mem-per-cpu=20000M

#SBATCH --ntasks-per-node=1

#SBATCH --time=1-00:00:00
 

# activate environment

cd /home/qb913274/BA/nnUNet

source /home/qb913274/anaconda3/etc/profile.d/conda.sh
conda activate nnFormer
 
# start training
export nnUNet_raw_data_base="/home/qb913274/BA/DATASET/nnUNet_raw_data_base"
export nnUNet_preprocessed="/home/qb913274/BA/DATASET/nnUNet_preprocessed"
export RESULTS_FOLDER="/home/qb913274/BA/DATASET/nnUNet_trained_models"


nnUNet_convert_decathlon_task -i ../DATASET/nnUNet_raw_data_base/nnUNet_raw_data/Task01_osteophytes
nnUNet_plan_and_preprocess -t 1 --verify_dataset_integrity
nnUNet_train 3d_fullres nnUNetTrainerV2 1 0
nnUNet_predict -i ../DATASET/nnUNet_raw_data_base/nnUNet_raw_data/Task001_osteophytes/imagesTs/ -o ../DATASET/nnUNet_raw_data_base/nnUNet_raw_data/Task001_osteophytes/inferTs/output -m 3d_fullres -f 0 -t 1 -chk model_best -tr nnUNetTrainerV2
nnFormer_evaluate_folder -ref ../DATASET/nnUNet_raw_data_base/nnUNet_raw_data/Task001_osteophytes/labelsTs -pred ../DATASET/nnUNet_raw_data_base/nnUNet_raw_data/Task001_osteophytes/pred -l 2
