import torchio as tio
import nibabel as nib
from pathlib import Path
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import math
import os

os.environ["KMP_DUPLICATE_LIB_OK"]  =  "TRUE"
num_training=191 # number of training cases
num_testing =49 # number of testing cases
# volume
volume_1=np.zeros(num_training) # training visible leg
volume_2=np.zeros(num_testing) # testing visible leg
volume_3=np.zeros(num_training) # training bone
volume_4=np.zeros(num_testing) # testing bone
# voxel
voxel_1=np.zeros(num_training) # training visible leg
voxel_2=np.zeros(num_testing) # testing visible leg
voxel_3=np.zeros(num_training) # training bone
voxel_4=np.zeros(num_testing) # testing bone


# comments are testing set

data_path1 = Path("...") # path of training visible leg
data_path2 = Path("...") # path of testing visible leg
data_path3 = Path("...") # path of training bone
data_path4 = Path("...") # path of testing bone

i=0
for filename in os.listdir(data_path2): # data_path1 for training set, data_path2 for testing set
    # # training set
    # file_path1=str(data_path1 / filename)
    # img1 = nib.load(file_path1)
    # nifti_numpy1 = img1.get_fdata()
    # pixdim1 = img1.header['pixdim']

    # testing set
    file_path2 = str(data_path2 / filename)
    img2 = nib.load(file_path2)
    nifti_numpy2 = img2.get_fdata()
    pixdim2 = img2.header['pixdim']


    # # training set
    # file_path3=str(data_path3 / filename)
    # img3 = nib.load(file_path3)
    # nifti_numpy3 = img3.get_fdata()
    # pixdim3 = img3.header['pixdim']

    # testing set
    file_path4=str(data_path4 / filename)
    img4 = nib.load(file_path4)
    nifti_numpy4 = img4.get_fdata()
    pixdim4 = img4.header['pixdim']


    # Compute volume sum

    # # training set
    # voxel_1[i] = np.count_nonzero(nifti_numpy1)
    # volume_1[i] = voxel_1[i] * pixdim1[1] * pixdim1[2] * pixdim1[3]

    ## testing set
    voxel_2[i] = np.count_nonzero(nifti_numpy2)
    volume_2[i] = voxel_2[i] * pixdim2[1] * pixdim2[2] * pixdim2[3]

    # # training set
    # voxel_3[i] = np.count_nonzero(nifti_numpy3)
    # volume_3[i] = voxel_3[i] * pixdim3[1] * pixdim3[2] * pixdim3[3]

    # testing set
    voxel_4[i] = np.count_nonzero(nifti_numpy4)
    volume_4[i] = voxel_4[i] * pixdim4[1] * pixdim4[2] * pixdim4[3]

    i=i+1

# compute avg value

# # training set
# avg1_volume=np.sum(volume_1)/num_training
# avg1_voxel=np.sum(voxel_1)/num_training

# testing set
avg2_volume=np.sum(volume_2)/num_testing
avg2_voxel=np.sum(voxel_2)/num_testing

# # training set
# avg3_volume=np.sum(volume_3)/num_training
# avg3_voxel=np.sum(voxel_3)/num_training

# testing set
avg4_volume=np.sum(volume_4)/num_testing
avg4_voxel=np.sum(voxel_4)/num_testing

# print result

# # training set
# print(avg1_volume)
# print(avg1_voxel)

# testing set
print(avg2_volume)
print(avg2_voxel)

# # training set
# print(avg3_volume)
# print(avg3_voxel)

# testing set
print(avg4_volume)
print(avg4_voxel)


