from pathlib import Path
import os
import numpy as np
import nibabel as nib

# Set path to directory
data_path = Path("...") # path of image

# Go through each file in directory
for filename in os.listdir(data_path):
    # Get path to file
    file_path = str(data_path / filename)
    img = nib.load(file_path)
    nifti_numpy = img.get_fdata()
    if np.all(nifti_numpy):
        print("!!!!!!!!!!!!!!!!!!!empty %s" % filename)
    else:
        print(filename)
